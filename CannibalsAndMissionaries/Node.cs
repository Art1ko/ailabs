﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CannibalsAndMissionaries
{
    public class Node : IEquatable<Node>
    {

        //cost from last node to current node
        private int GCost { get; set; }
        // heuristic cost 
        private int HCost { get; set; }
        //Total cost
        private int FCost { get; set; }
        private readonly State _state;
        private readonly Node _parent;
        private readonly bool _toGoal;
        private readonly int moveDistance = 1;


        private readonly Dictionary<State, int> _possibleMovesWithTheirHeuristicValues = new Dictionary<State, int>(new MyDictionaryComparer())
        {
            { new State(1, 0, 0), 1},
            { new State(2, 0, 0), 1},
            { new State(1, 1, 0), 2},
            { new State(1, 0, 1), 2},
            { new State(0, 1, 1), 3},
            { new State(0, 0, 1), 3}
        };

        public Node(State state = null, Node parent = null, bool toGoal = true)
        {
            _state = state ?? new State(3, 2, 1);
            _parent = parent;
            _toGoal = toGoal;
        }

        private Node Transition(State move)
        {
            return new Node(_toGoal ? _state - move : _state + move, this, !_toGoal);
        }

        private IEnumerable<Node> Children
        {
            get
            {
                return _possibleMovesWithTheirHeuristicValues.Keys
                    .Select(Transition)
                    .Where(node => node._state.IsValid);
            }
        }

        public IEnumerable<Node> PathToRoot
        {
            get
            {
                var node = this;
                while (true)
                {
                    yield return node;
                    node = node._parent;
                    if (node._parent == null)
                    {
                        yield return node;
                        break;
                    }
                }
            }
        }

        private int CalcuteMoves()
        {
            var node = this;
            var counter = 0;
            while (node._parent != null)
            {
                counter++;
                node = node._parent;
            }
            return counter;
        }



        public Node FindSolution()
        {
            var path = new Node();
            // List of nodes to be traversed
            var openList = new List<Node>(Children);
            //List of already traversed nodes
            var examined = new List<Node>();
            while (openList.Any())
            {
                openList = openList.OrderBy(child => child._possibleMovesWithTheirHeuristicValues.Values.Min() +
                moveDistance).ToList();
                var node = openList.First();
                if (node._state.IsGoal)
                {
                    path = node;
                    break;
                }
                openList.Remove(node);
                examined.Add(node);
                foreach (var child in node.Children)
                {
                    if (examined.Contains(child))
                        continue;

                    var Cost = node.GCost + moveDistance;
                    if (openList.Contains(child) && child.GCost < node.GCost)
                        openList.Remove(child);
                    if (examined.Contains(child) && child.GCost < node.GCost) ;
                    if (!examined.Contains(child) && !openList.Contains(child))
                    {
                        openList.Add(child);
                        child.GCost = Cost;
                        var move = child._toGoal ?   child._state - node._state : node._state - child._state;
                        child.HCost = child._possibleMovesWithTheirHeuristicValues[move];
                        child.FCost = child.GCost + child.HCost;
                    }
                }

            }
            return path;
        }

        public string Direction => _toGoal ? "->" : "<-";

        public IEnumerable<State> Statements
        {
            get
            {
                return PathToRoot
                    .Select(node => node._state);
            }
        }

        public bool Equals(Node other)
        {
            return other != null && _state.Equals(other._state) && _toGoal == other._toGoal;
        }

        public string Formatted(State boat = null)
        {
            var dirStr = boat == null ? "==" : Direction + "Boat" + boat + Direction;
            return "Left" + _state + dirStr + "Right" + _state.Reverse();
        }
    }
}
