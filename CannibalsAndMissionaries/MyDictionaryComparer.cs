﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CannibalsAndMissionaries
{
    class MyDictionaryComparer : IEqualityComparer<State>
    {
        public bool Equals(State x, State y)
        {
            return x.People == y.People && x.Monkeys == y.Monkeys && x.Monkeys == y.Monkeys;
        }

        public int GetHashCode(State x)
        {
            return x.People.GetHashCode() + x.Monkeys.GetHashCode() + x.Ape.GetHashCode();
        }
    }
}
