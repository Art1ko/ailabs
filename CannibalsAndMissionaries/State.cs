﻿using System;

namespace CannibalsAndMissionaries
{
    public class State : IEquatable<State>
    {
        private const int MaxPeople = 3;
        private const int MaxMonkeys = 2;
        private const int MaxApe= 1;

        public int People { get; }
        public int Monkeys { get; }
        public int Ape { get; }

        public State(int people, int monkeys, int ape)
        {
            People = people;
            Monkeys = monkeys;
            Ape = ape;
        }

        public bool IsValid
        {
            get
            {
                var correctManipulation = People >= 0 && Monkeys >= 0 
                    && MaxPeople - People >= 0 
                    && MaxMonkeys - Monkeys >= 0 
                    && MaxApe - Ape >= 0;
                var wrongSide = People == 0 || People >= Monkeys + Ape;
                var rightSide = MaxPeople - People == 0 || MaxPeople - People >= MaxMonkeys - Monkeys + MaxApe - Ape;

                return correctManipulation && wrongSide && rightSide;
            }
            set { }
        }

        public bool IsGoal => People == 0 && Monkeys == 0 && Ape == 0;

        public State Reverse() => new State(MaxPeople - People, MaxMonkeys - Monkeys, MaxApe - Ape);

        public State Abs() => new State(Math.Abs(People), Math.Abs(Monkeys), Math.Abs(Ape));

        public static State operator -(State s1, State s2)
        {           
            return new State(s1.People - s2.People < 0? 1 : s1.People - s2.People, s1.Monkeys - s2.Monkeys, s1.Ape - s2.Ape < 0? 0 : s1.Ape - s2.Ape);
        }

        public static State operator +(State s1, State s2)
        {
            return new State(s1.People + s2.People, s1.Monkeys + s2.Monkeys, s1.Ape + s2.Ape);
        }

        public override string ToString()
        {
            return "(P" + People + " M" + Monkeys + " A" + Ape + ")";
        }

        public bool Equals(State other)
        {
            return other != null && People == other.People && Monkeys == other.Monkeys && Ape == other.Ape;
        }
    }
}
